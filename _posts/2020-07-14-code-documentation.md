---
layout: post
title:  "Hosting Doxygen Generated Files"
subheading: Using Gitlab CI/CD tools to automate documentation workflow.
author: Luke
categories:
banner:
  video:
  loop:
  volume:
  start_at:
  image: "/assets/images/banners/federico-di-dio-photography-Wstln0400pE-unsplash.jpg"
  alt: "Photo by Federico Di Dio photography on Unsplash</a>"
  opacity:
  background: "#000"
  height: "100vh"
  min_height: "38vh"
  heading_style: "font-size: 4.25em; font-weight: bold; text-decoration: underline"
  subheading_style: "color: gold"
tags: gitlab doxygen
sidebar: []
---

## Auto-Generated Documentation

Doxygen is a fantastic tool for creating high-quality, in-depth source code documentation almost straight out of the box. At the most basic level, Doxygen scans your source code, looking for a unique comment string, extracts the relevant information and compiles them into a LaTeX and HTML format. For more information, check out the Doxygen [website](https://www.doxygen.nl/index.html). These files could be directly attached to your repository, but I would argue that defeats the purpose of auto-generation.

One option is to host the available HTML files online. Unfortunately, if your not immediately familiar with web design and hosting, like myself, this could prove troublesome.

## Enter GitLab CI/CD

If you are using GitLab for your revision control, you can take advantage of its build-in *Continuous Integration and Continuous Deployment* (CI/CD) Pipeline to run automated tasks when committing new code. If you're like me, you have been avoiding GitLab's CI/CD features out of fear and uncertainty, but to my surprise, Gitlab makes using these tools insanely easy; for most tasks, its a simple point and click functionality.

Here I will show how to create a job file to generate and post your Doxygen generated files.

1. From the repository web UI, add a **new file**.

2. From the *Select Template Type* drop-down, select **.gitlab-ci.yml**

3. From the *Apply Template* drop-down, select **Doxygen**

Slight modifications to the script may be needed depending on the structure of your project, mainly where your Doxygen related files are.

Now anytime you commit/push to your master branch, GitLab will automatically run this script, and on its success, it'll post your HTML to its own unique URL; The URL can be found under **Settings >> Pages**

See example [here](https://gitlab.com/libs2/c/data-struct/queue)